/***** Function declaration и function expression *****/

// Function declaration
// Доступна до объявления. Не передает контекст.
function foo(arg) {
  console.log(arg);
}

foo('Function declaration');

// Function expression
// Недоступны до объявления. Передают контекст (см. context.js).
let bar = function(arg) { 
  console.log(arg);
};
bar('Function expression v1');

let baz = (arg) => {
  console.log(arg);
};
baz('Function expression v2');
