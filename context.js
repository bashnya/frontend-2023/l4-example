/***** Передача контекста *****/

// Объект со свойствами разных типов
let user = {
  firstName: 'Иван',
  lastName: 'Иванов',
  age: 10,
  // Свойство типа Function
  sayHi: function() {
    // this - обращение к самому себе (объекту user)
    console.log('Привет, меня зовут ' + this.firstName);
  },
};

user.sayHi(); // Привет, меня зовут Иван



// Передаем первую функцию аргументом во вторую
// Вызываем первую функцию внутри второй
function someoneSaysHi(sayHiFunction) {
  sayHiFunction();
}

// Контекст не передается
// (внутри someoneSaysHi неизвестно что такое this)
someoneSaysHi(user.sayHi); // Привет, меня зовут undefined
// 1 способ передать контекст
someoneSaysHi(user.sayHi.bind(user)); // Привет, меня зовут Иван
// 2 способ передать контекст
someoneSaysHi(() => user.sayHi()); // Привет, меня зовут Иван
