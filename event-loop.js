/***** Event loop и промисы *****/

// ----- Зачем все это нужно (пример) ----- 
function increment() {
  const counterElement = document.getElementById('counter');

  counterElement.innerHTML = Number.parseInt(counterElement.innerHTML) + 1;
}

// Первые 5 секунд кнопка инкрементирования работает.
// Затем на время цикла кнопка подвисает (как и весь интерфейс),
// потому что стек заблокирован.
// После 'finished' в консоли, кнопка снова работает.
setTimeout(() => { 
  for (let i = 0; i <= 1e9; i++) {
    if (i % 1e8 === 0) {
      console.log(i);
    }
  }
  console.log('finished');
}, 5000);

// ----- -----


// ----- Колбэки и промисы -----

// Елочка, код растет в ширину. Ад колбэков. (1)
setTimeout(function() {
  setTimeout(function() {
    setTimeout(function() {
      setTimeout(function() {
        console.log('timeouts finished');
      }, 10000);
    }, 8000);
  }, 5000);
}, 1000);

// Делаем setTimeout, который будет возвращать промис
function promisifiedSetTimeout(timeout) {
  return new Promise(
    resolve => setTimeout(resolve, timeout),
  );
} 

// То же, что (1), но промисами. Код в ширину не растет.
// Такая инструкция (через точки) называется чейнингом. (2)
promisifiedSetTimeout(1000)
  .then(promisifiedSetTimeout(5000))
  .then(promisifiedSetTimeout(8000))
  .then(promisifiedSetTimeout(10000))
  .then(console.log('timeouts finished'));

// То же, что (2), но более гибкий вариант.
// await ждет выполнения промиса и только потом продолжает функцию.
// await можно писать только внутри функции с модификатором async. (3)
async function foo() {
  const timeouts = [1000, 5000, 8000, 10000];
  for (const timeout of timeouts) {
    await promisifiedSetTimeout(timeout);
  }
  console.log('timeouts finished');
}
