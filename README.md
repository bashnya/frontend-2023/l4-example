# БАШНЯ. Курс по фронту. Код к занятию 4

### Разобрали на занятии
* Замыкания и лексическое окружения функции
* Контекст функции
* Function declaration и function expression
* Event loop
* Колбэки и промисы

### Материалы
* [Выступление про Event loop](https://www.youtube.com/watch?v=8aGhZQkoFbQ) &mdash; 
  крутая визуализация концепции, лучшее ее объяснение (на нашему мнению)
* [Инструмент из выступления выше](http://latentflip.com/loupe/) &mdash; можно поиграться с Event loop
* [Выступление про тонкости Event loop в браузере](https://www.youtube.com/watch?v=cCOL7MC4Pl0) &mdash; продвинутая
  версия Event loop + несколько другой вариант визуализации
* Неплохая [статейка с Хабра про промисы](https://habr.com/ru/articles/439746/)
* [Тут](https://habr.com/ru/companies/skillbox/articles/651781/) много примеров `async/await`
* Замыкания это не только про JS. Хорошо про них написано на 
  [Википедии](https://ru.wikipedia.org/wiki/%D0%97%D0%B0%D0%BC%D1%8B%D0%BA%D0%B0%D0%BD%D0%B8%D0%B5_(%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5))
* [Про промисы](https://learn.javascript.ru/async), [function expression](https://learn.javascript.ru/function-expressions) и 
  [замыкания](https://learn.javascript.ru/closure), а так же наверное про все основные концепции ванильного JS можно почитать 
  в [этом замечательном учебнике](https://learn.javascript.ru)
* Все что разобрали само собой есть на [MDN](https://developer.mozilla.org/ru/)
