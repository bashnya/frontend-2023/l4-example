/***** Замыкания *****/

function createCounter() {
  let count = 0; // К этой переменной снаружи доступа нет

  return function() {
    return count++; // но взаимодействовать и вернуть ее можно
  }
}

// Две независимые сущности
const counter1 = createCounter();
const counter2 = createCounter();

console.log(counter1()) // 0
console.log(counter1()) // 1

console.log(counter2()) // 0 (независимо от counter1)
